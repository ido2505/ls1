
#include "stdafx.h"
#include "stack.h"

void add(stack* s, unsigned int element) 
{
	if (s->value == -1)
	{
		s->value = element;
	}
	else
	{
		stack* temp = s;
		while (temp->next)
		{
			temp = temp->next;
		}
		temp->next = new stack();
		initStack(temp->next);

		temp = s;
		while (temp->next)
		{
			temp->next->value = temp->value;

			temp = temp->next;
		}

		s->value = element;
	}
}

int remove(stack * s) 
{
	int ans = -1;

	if (s->next == NULL) 
	{
		ans = s->value;
		s->value = -1;
	}
	else if(s != NULL) 
	{
		stack* temp = s;
		ans = s->value;
		while (temp->next->next)
		{
			temp->value = temp->next->value;
			temp = temp->next;
		}
		delete(temp->next);
		temp->next = NULL;
	}
	return ans;
}

void push(stack* s, unsigned int element) 
{
	if (s->value == -1) 
	{
		s->value = element;
	}
	else 
	{
		stack* temp = s;
		while (temp->next)
		{
			temp = temp->next;
		}
		temp->next = new stack();
		initStack(temp->next);
		temp->next->value = element;
	}	
}

int pop(stack* s) 
{
	int ans = -1;
	if (s->next == NULL) 
	{
		ans = s->value;
		s->value = -1;
	}
	else if (s->next->next == NULL) 
	{
		ans = s->next->value;
		delete(s->next);
		s->next = NULL;
	}
	else 
	{
		stack* temp = s;
		while (temp->next->next)
		{
			temp = temp->next;
		}
		ans = temp->next->value;
		delete(temp->next);
		temp->next = NULL;
	}
	return ans;
}

void initStack(stack* s) 
{
	s->next = NULL;
	s->value = -1;
}

void cleanStack(stack* s) 
{
	stack* temp1 = s->next; 
	stack* temp2 = s->next;

	while (temp1) 
	{
		temp2 = temp1;
		temp1 = temp1->next;
		delete(temp2);
	}
	initStack(s);
}
#ifndef STACK_H
#define STACK_H


/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int value;
	stack * next;
} stack;

//1
void add(stack* s, unsigned int element);
int remove(stack * s);

//2
void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

#endif // STACK_H

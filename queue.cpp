#include "stdafx.h"
#include "queue.h"



void initQueue(queue* q, unsigned int size)
{
	q->_maxSize = size;
	q->_count = 0;
	q->_elements = new int[size];

	for (int i = 0; i < size; i++)
	{
		q->_elements[i] = -1;
	}
}

void cleanQueue(queue* q)
{
	for (int i = 0; i < q->_maxSize; i++)
	{
		q->_elements[i] = -1;
	}
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->_count != q->_maxSize)
	{
		int flag = false;
		for (int i = 0; i < q->_maxSize && !flag; i++)
		{
			if (q->_elements[i] == -1)
			{
				q->_elements[i] = newValue;
				flag = true;
			}
		}
		q->_count++;
	}
}

int dequeue(queue* q)
{
	int ans = -1;

	if (q->_count != 0)
	{
		ans = q->_elements[0];

		for (int i = 0; i < q->_maxSize - 1; i++)
		{
			q->_elements[i] = q->_elements[i + 1];
		}
		q->_count--;
	}

	return ans;
}
#include "stdafx.h"
#include "utils.h"
#include "stack.h"
#include <iostream>

void reverse(int* nums, unsigned int size) 
{
	stack* s = new stack();
	initStack(s);

	for (int i = 0; i < size; i++) 
	{
		push(s, nums[i]);
	}
	for (int j = 0; j < size; j++) 
	{
		nums[j] = pop(s);
	}
}


int* reverse10() 
{
	int size = 10;
	int* arr = new int[size];

	stack* s = new stack();
	initStack(s);

	for (int i = 0; i < size; i++) 
	{
		std::cin >> arr[i];
		push(s, arr[i]);
	}

	for (int j = 0; j < size; j++)
	{
		arr[j] = pop(s);
	}

	return arr;
}